# Instalação:

primeiro crie o banco mobly, pelo prompt do mysql

`mysql -u root -p`

`create database mobly`

depois copie o arquivo .env.example para .env

para inicar o projeto va até a pasta local e rode o comando `php artisan migrate`

rode o comando `php artisan import:data_api`

depois rode o comando para subir o servidor: php artisan serve

#Rotas das apis:

todos os usuários:

`http://localhost:8000/api/users/`

usuário específico:

`http://localhost:8000/api/users/{id}`


posts de um usuário específico:

`http://localhost:8000/api/users/{id}/posts`

todos os posts:
`http://localhost:8000/api/posts/`

#Rotas web

listagem:

`http://localhost:8000/users`
