@extends('layout.layout')

@section('content')
    <h1>Add New User</h1>
    <hr>
     <form action="/users" method="post">
     {{ csrf_field() }}
         <div class="form-group">
             <label for="name">Name</label>
             <input type="text" class="form-control" id="name" name="name">
         </div>
      <div class="form-group">
        <label for="username">Username</label>
        <input type="text" class="form-control" id="username"  name="username">
      </div>
     <div class="form-group">
         <label for="email">Email</label>
         <input type="email" class="form-control" id="email"  name="email">
     </div>
     <div class="form-group">
         <label for="website">Website</label>
         <input type="text" class="form-control" id="website"  name="website">
     </div>
     <div class="form-group">
         <label for="website">Website</label>
         <input type="text" class="form-control" id="website"  name="website">
     </div>
     <div class="form-group">
         <label for="phone">Phone</label>
         <input type="text" class="form-control" id="phone"  name="phone">
     </div>
     <div class="form-group">
         <label for="company_id"></label>
         <select name="company_id" id="company_id">
             @php
                 foreach(App\Company::get() as $company){
             @endphp
             <option value="{{$company->id}}"  >{{$company->name}}</option>
             @php
                 }
             @endphp
         </select>
     </div>
     @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
      @endif
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection