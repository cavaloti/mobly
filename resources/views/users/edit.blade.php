@extends('layout.layout')

@section('content')
    <h1>Edit User</h1>
    <hr>
     <form action="{{url('users', [$user->id])}}" method="POST">
     <input type="hidden" name="_method" value="PUT">
     {{ csrf_field() }}
      <div class="form-group">
        <label for="name">Name</label>
        <input type="text" value="{{$user->name}}" class="form-control" id="name"  name="name" >
      </div>
      <div class="form-group">
        <label for="username">Username</label>
        <input type="text" value="{{$user->username}}" class="form-control" id="username" name="username" >
      </div>
     <div class="form-group">
         <label for="email">Email</label>
         <input type="text" value="{{$user->email}}" class="form-control" id="email" name="email" >
     </div>
     <div class="form-group">
         <label for="phone">Phone</label>
         <input type="text" value="{{$user->phone}}" class="form-control" id="phone" name="phone" >
     </div>
     <div class="form-group">
         <label for="website">Website</label>
         <input type="text" value="{{$user->website}}" class="form-control" id="website" name="website" >
     </div>
     <div class="form-group">
         <label for="company_id"></label>
         <select name="company_id" id="company_id">
             @php
                 foreach(App\Company::get() as $company){

                     $selected = '';
                     if($user->company_id == $company->id){
                        $selected = 'selected="selected"';
                     }
             @endphp
                 <option value="{{$company->id}}"  {{$selected}} >{{$company->name}}</option>
             @php
             }
             @endphp
         </select>
     </div>
      @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
      @endif
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection