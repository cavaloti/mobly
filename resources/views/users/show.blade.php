@extends('layout.layout')

@section('content')
            <h1>Showing User {{ $user->title }}</h1>

    <div class="jumbotron text-center">
        <p>
            <strong>Username:</strong> {{ $user->title }}<br>
            <strong>Email:</strong> {{ $user->description }}
        </p>
    </div>
@endsection