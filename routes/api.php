<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('users', 'UserController@listAll');
Route::get('users/{id}', 'UserController@detail');
Route::get('users/{id}/posts', 'PostsController@userPosts');
Route::get('posts', 'PostsController@index');