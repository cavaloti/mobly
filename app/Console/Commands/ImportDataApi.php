<?php

namespace App\Console\Commands;

use App\Company;
use App\Posts;
use App\UserAddress;
use Illuminate\Console\Command;
use App\User;

class ImportDataApi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:data_api';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import data from api to mysql';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }




    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


        // retrieve users data from api
        $this->getUsersDataApi();

        //retrieve posts data from api
        $this->getUsersPostsDataApi();

        $this->comment('finished');

        return [];
    }

    public function getUsersPostsDataApi(){
        //get users posts data
        $usersPostsApiUrl = env('USERS_POSTS_API_URL');
        $usersPostsDataApiUrl = json_decode(file_get_contents($usersPostsApiUrl));

        foreach ($usersPostsDataApiUrl as $userPostData){
            $posts = new Posts();
            $posts->user_id = $userPostData->userId;
            $posts->title = $userPostData->title;
            $posts->body = $userPostData->body;
            $posts->save();
        }

    }

    public function getUsersDataApi(){
        // get users api data
        $usersApiUrl = env('USERS_API_URL');
        $usersDataApi = json_decode(file_get_contents($usersApiUrl));

        foreach ($usersDataApi as $userData){

            $user = User::firstOrNew(['email'=> $userData->email]);
            $user->name       = $userData->name;
            $user->username   = $userData->username;
            $user->email      = $userData->email;
            $user->phone      = $userData->phone;
            $user->website    = $userData->website;
            $user->company_id = $this->commitCompany( $userData->company );
            $user->save();

            $this->commitUserAddress($userData->address, $user->id);
        }


    }

    protected function  commitCompany($companyData){
        $company = new Company();
        $company = $company->firstOrNew(['name' => $companyData->name]);
        $company->catch_phrase = $companyData->catchPhrase;
        $company->bs = $companyData->bs;
        $company->save();
        return $company->id;
    }

    protected function commitUserAddress($address, $userId){
        $userAdress = new UserAddress();
        $userAdress = $userAdress->firstOrNew(
            [
                'zipcode'=>$address->zipcode,
                'user_id'=>$userId,
                'street'=>$address->street,
                'suite'=>$address->suite
            ]
        );
        $userAdress->street    = $address->street;
        $userAdress->suite     = $address->suite;
        $userAdress->city      = $address->city;
        $userAdress->zipcode   = $address->zipcode;
        $userAdress->lat       = $address->geo->lat;
        $userAdress->lng       = $address->geo->lng;
        $userAdress->user_id   = $userId;

        return $userAdress->save();
    }
}
